package mobilebankinggateway.common;

import lombok.experimental.UtilityClass;

@UtilityClass
public class StringConstants {
    public static final String SESSION_ID = "sessionId";
    public static final String PAYLOAD = "payload";

}
